package com.kakaogames.game.api.config.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.ResourceUrlEncodingFilter;
import org.springframework.web.servlet.resource.VersionResourceResolver;

@EnableWebMvc
@Configuration
public class ResourceResolverConfig implements WebMvcConfigurer {
    /*
        TODO. 버저닝된 리소스에 대한 접근 처리.
        리소스 변경시 자동 변경된 버저닝 값이 설정됨.
        ex)
        <script th:src="@{/res/js/1.js}"></script>
        <script src="/res/js/1-af2434e461ffaaca01d072bda7b89b85.js"></script>
    */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/favicon.ico")
                .addResourceLocations("classpath:/favicon.ico");

        registry.addResourceHandler("/res/**")
                .addResourceLocations("classpath:/res/")
                .resourceChain(false)
                .addResolver(new VersionResourceResolver().addContentVersionStrategy("/**"));
    }

    @Bean
    public ResourceUrlEncodingFilter resourceUrlEncodingFilter() {
        return new ResourceUrlEncodingFilter();
    }
}