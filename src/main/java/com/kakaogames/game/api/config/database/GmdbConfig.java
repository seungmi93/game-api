package com.kakaogames.game.api.config.database;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
public class GmdbConfig {
    @Bean
    @ConfigurationProperties(prefix = "datasource.gmdb-master")
    public DataSource gmDbMasterDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties(prefix = "datasource.gmdb-slave")
    public DataSource gmDbSlaveDataSource() {
        return DataSourceBuilder.create().build();
    }

}