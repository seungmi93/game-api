package com.kakaogames.game.api.domain.service;

import com.kakaogames.game.api.domain.entity.AllowedIp;
import com.kakaogames.game.api.domain.repository.AllowedIpRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
public class AllowedIpService {

    @Autowired
    private AllowedIpRepository allowedIpRepository;

    @Cacheable("ipAuthCache")
    public boolean isAllowedIp(String clientIp) {
        if (StringUtils.isEmpty(clientIp)) {
            return false;
        }
        String cClassClientIp = clientIp.substring(0, clientIp.lastIndexOf("."));

        AllowedIp allowedIp = allowedIpRepository.findByIp(cClassClientIp);
        if (null != allowedIp) {
            return true;
        }

        allowedIp = allowedIpRepository.findByIp(clientIp);
        return (null != allowedIp);
    }
}
