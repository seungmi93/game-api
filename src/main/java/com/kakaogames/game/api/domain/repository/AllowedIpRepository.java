package com.kakaogames.game.api.domain.repository;

import com.kakaogames.game.api.domain.entity.AllowedIp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface AllowedIpRepository extends JpaRepository<AllowedIp, Long> {

    @Transactional(readOnly = true)
    @Query("SELECT acl FROM gameapi_acl acl WHERE acl.ip = :ip")
    AllowedIp findByIp(@Param("ip") String ip);
}
