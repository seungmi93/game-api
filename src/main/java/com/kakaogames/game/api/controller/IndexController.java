package com.kakaogames.game.api.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    @RequestMapping(value = "/")
    public String Index(Model model) {
        // TODO. 1. model attribute.
        model.addAttribute("data", "data");

        return "index";
    }
}