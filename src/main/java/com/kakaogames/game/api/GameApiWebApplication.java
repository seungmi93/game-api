package com.kakaogames.game.api;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@EnableCaching
@SpringBootApplication
public class GameApiWebApplication {
    public static void main(String[] args) {
        SpringApplication.run(GameApiWebApplication.class, args);
    }
}
