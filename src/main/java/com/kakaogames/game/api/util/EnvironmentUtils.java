package com.kakaogames.game.api.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EnvironmentUtils {

    private static String activeProfile;

    @Value("${spring.profiles.active}")
    public void setActiveProfile(String activeProfile) {
        this.activeProfile = activeProfile;
    }

    public static boolean isLocalOrDev() {
        return "dev".equals(activeProfile) || "local".equals(activeProfile);
    }

    public static boolean isDev() {
        return "dev".equals(activeProfile);
    }

    public static boolean isProduction() {
        return "production".equals(activeProfile);
    }

}
