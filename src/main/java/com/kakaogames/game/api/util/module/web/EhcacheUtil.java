package com.kakaogames.game.api.util.module.web;

import org.springframework.boot.CommandLineRunner;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

@Component
public class EhcacheUtil implements CommandLineRunner {

    private final CacheManager cacheManager;

    public EhcacheUtil(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    @Override
    public void run(String... strings) throws Exception {
    }
}