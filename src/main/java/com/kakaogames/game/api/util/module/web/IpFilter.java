package com.kakaogames.game.api.util.module.web;

import com.kakaogames.game.api.domain.service.AllowedIpService;
import com.kakaogames.game.api.util.EnvironmentUtils;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class IpFilter implements Filter {

    private AllowedIpService allowedIpService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (EnvironmentUtils.isLocalOrDev()) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        String requestUri = httpServletRequest.getRequestURI();
        if (isExcludeURI(requestUri)) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        String remoteAddr = httpServletRequest.getRemoteAddr();
        if (allowedIpService.isAllowedIp(remoteAddr)) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        httpServletResponse.sendRedirect("/unauthorized_address");
    }

    @Override
    public void destroy() {
    }

    private boolean isExcludeURI(String requestUri) {
        if (requestUri.contains("/webjars/") || requestUri.contains("/static/") || requestUri.contains("/favicon.ico") || requestUri.contains("/unauthorized_address")) {
            return true;
        }

        return false;
    }
}

