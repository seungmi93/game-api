### [GAME-API]

##### [정보]

* GroupId: com.kakaogames
* ArtifactId: game-api
* gradle: Use default gradle wrapper

##### [의존성]

* java: openJDK 9.0.4
* spring: 5.1.5.RELEASE
* spring-boot: 2.1.3.RELEASE

##### [설치 - intellij]

* plugins
    * Shift *2 > Plugins > Browse repositories... > Search for "vue.js" > Install Plugin
    * Shift *2 > Plugins > Browse repositories... > Search for "lombok" > Install Plugin    
    
* settings
    * File > Settings > Languages & Frameworks > Javascript > Javascript version > ECMA6
    * File > Settings > Build, Execution, Deployment > Compiler > Annotation Processors > Click Enable Annotation Processing
    * File > Settings > Editors > Inspections > Spring > Warning (check)

1. File > Open > game-api
2. Edit Configurations > Spring boot
	* Main class: com.kakaogames.game.api.GameApiWebApplication
	* JRE: openJDK 9
	* Spring Boot
		* On 'Update' action: Update resources
		* On frame deactivation: Update classes and resources
		
3. RUN > http://localhost:10998

##### [접속 환경]

[local]

* http://localhost:10998

##### [wiki]
* https://wiki.daumkakao.com/pages/viewpage.action?pageId=600111093